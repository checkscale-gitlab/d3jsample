FROM node:alpine
EXPOSE 8080
WORKDIR '/app'
COPY ./package.json ./
RUN npm install
COPY ./src/ ./src/
COPY ./webpack.config.js ./webpack.config.js
COPY ./public/ ./public/
RUN npm run build
CMD npm start